def romanos(alg):
    values = {
        'I': 1,
        'V': 5,
        'X': 10,
        'L': 50,
        'C': 100,
        'D': 500,
        'M': 1000,
    }
    
    results = [values[e] * -1 if len(alg) > c + 1 and values[e] < [values[i] 
                for i in alg][c + 1] else values[e] for c, e in enumerate(alg)]
    
    return sum(results)